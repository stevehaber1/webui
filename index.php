<!DOCTYPE html>
<?php
    if (isset($_POST[stop])) {
        shell_exec('sudo pkill pifm');
        echo exec( 'sh /home/pi/lightshowpi/bin/stop_music_and_lights');
    }

    if(isset($_POST[playlist])){
        shell_exec('sh /home/pi/lightshowpi/bin/start_music_and_lights');
    }

    if(isset($_POST[allon])){
        shell_exec('sudo python /home/pi/lightshowpi/py/hardware_controller.py --state=on');
    }

    if(isset($_POST[alloff])){
        shell_exec('sudo python /home/pi/lightshowpi/py/hardware_controller.py --state=off');
    }

    if(isset($_POST[flash])){
        shell_exec('sudo python /home/pi/lightshowpi/py/hardware_controller.py --state=flash');
    }

    if(isset($_POST[fade])){
        shell_exec('sudo python /home/pi/lightshowpi/py/hardware_controller.py --state=fade');
    }

    if(isset($_POST[reboot])){
        shell_exec('sudo reboot');
    }
    if(isset($_POST[light1On])){
        shell_exec('sudo python /home/pi/lightshowpi/py/hardware_controller.py --state=1on');
    }
    if(isset($_POST[light1Off])){
        shell_exec('sudo python /home/pi/lightshowpi/py/hardware_controller.py --state=1off');
    }

    $dir = '/home/pi/music/*.mp3';

    foreach(glob($dir) as $mp3){
        $k = pathinfo($mp3);
        $m = $k['basename'];
        $v = explode(".",$k['basename']);
        $v4 = explode('-',$m);

        if(isset($_POST[$v[0]])){
            shell_exec('sudo pkill pifm');
            shell_exec('sudo killall python');
            shell_exec('sudo python /home/pi/lightshowpi/py/synchronized_lights.py --file=/home/pi/music/'.$v[0].'.mp3');
        }
    }


?>

<html>
    <head>
        <meta charset="utf-8" />
        <title>Lightshow Pi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="apple-touch-icon" href="/apple-touch-icon-iphone.png" />
        <link rel="apple-touch-startup-image" href="/startup.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="stylesheet" type="text/css" href="css/jquery.sidr.dark.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="/sidr/jquery.sidr.min.js"></script>
        <script>
        $(document).ready(function() {
        setInterval(function() {
            $('#sidr-id-now-playing').load('nowplaying.php');
        }, 1000);
        });
        </script>
        <!-- Slideout Menus -->
        <script>
        $(document).ready(function() {
            $('#left-menu').sidr({
                name: 'sidr-left',
                side: 'left', // By default
                source: '#sidr-left'
            });
            $('#right-menu').sidr({
                name: 'sidr-right',
                side: 'right',
                source: '#sidr-right',
            });
        });
        </script>
        <!-- Play/+Playlist Toggle -->
        <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            $('.songMain').click(function() {
                 $(this).prevUntil('.songSec').hide();
                 $(this).nextUntil('.songSec').hide();
            });
            $('.songMain').click(function() {
                $(this).next('.songSec').toggle();
            });
        });
        </script>

    </head>

    <body>

        <div id="topBar">
            <div id="menuLeft">
                <a id="left-menu" href="#left-menu">
                    <img src="images/menu.png" />
                </a>
            </div>
            <div id="logo">
                <a href="index.php">
                    <img src="images/logo.png" />
                </a>
            </div>
            <div id="menuRight">
                <a id="right-menu" href="#right-menu">
                    <img src="images/playing.png" />
                </a>
            </div>
        </div>

        <div id="musicList">
            <?php
                foreach(glob($dir) as $mp3)
                {
                    $k = pathinfo($mp3);
                    $m = $k['basename'];
                    $v = explode(".",$k['basename']);
                    $v2 = str_replace ("_", " ", $v);
                    $v3 = $v2;
                    $v4 = explode('-',$m);
                    $v5 = str_replace ("_", " ", $v4);
                    $v6 = str_replace (".mp3", "", $v5);
                    $a = 'add';
                    $r = 'remove';
                    $va = $v[0] . '' . $a;
                    $vr = $v[0] . '' . $r;
                    $a = '/home/pi/music/';
                    $tab = "\t";
                    echo "<div class='songMain'><span class='song'>".$v6[0]." </span><br><span class='artist'>".$v6[1]."</span><img class='aRight'src='images/right.png'/></div>
                    <div class='songSec'><span class='play'><form method='post'><button name=".$v[0].">Play</button></form></span>
                    <span class='add'><form method='post'><button name=".$va.">+ Playlist</button></form></span></div>";
                    if(isset($_POST[$va])){
                        $myfile = '../../home/pi/music/playlist/.playlist';
                        $current = file_get_contents($myfile);
                        // Append a new person to the file
                        $current .= $v6[0].''.$v6[1].''.$tab.''.$a.''.$m;
                        // Write the contents back to the file
                        file_put_contents($myfile, trim($current).PHP_EOL);
                        fclose($myfile);
                        shell_exec('sudo');
                    }
                }
            ?>
        </div>
        <div id="bottomBar">
            <div class="but" id="but1">
                <form method="post">
                    <button class="playlist" name="playlist">
                        <img src="images/playlist.png" />
                        <br>Play All
                    </button>
                </form>
            </div>
            <div class="but" id="but2">
                <form method="post">
                    <button name="allon">
                        <img src="images/on.png" />
                        <br>All On
                    </button>
                </form>
            </div>
            <div class="but" id="but3">
                <form method="post">
                    <button name="alloff">
                        <img src="images/off.png" />
                        <br>All Off
                    </button>
                </form>
            </div>
            <div class="but" id="but4">
                <form method="post">
                    <button class="stop" name="stop">
                        <img src="images/stop.png" />
                        <br>Stop
                    </button>
                </form>
            </div>
        </div>

        <div id="sidr-left" style="display:none;">
            <div class="sidr-inner">
                <h2>Controls</h2>

                <form method="post">
                    <button name="flash">
                        Flash
                    </button>
                    <button name="fade">
                        Fade
                    </button>
                    <button name="reboot">
                        Reboot
                    </button>
                </form>
                <h2>System Info</h2>
                 <?php
                    echo "Uptime: ". shell_exec('uptime'). "";
                    $f = fopen("/sys/class/thermal/thermal_zone0/temp","r");
                    $temp = fgets($f);
                    $ftemp = $temp*9/5+32;
                    $sunset = date_sunset(time(), SUNFUNCS_RET_STRING, 37.9715590, -87.5710900, 90, -6);
                        date_default_timezone_set('America/Chicago');
                    $currentTime = date('H:i', time());
                    echo '<br>Pi temp is '.round($temp/1000);
                    echo 'c ('.round($ftemp/1000);
                    echo 'f)';
                    echo '<br>Current Time: '.$currentTime;
                    echo '<br>Sunset: '.$sunset;
                    fclose($f);
                ?>
            </div>
        </div>

        <div id="sidr-right" style="display:none;">
            <div class="sidr-inner">
                <h2>Now Playing</h2>
                <div id="now-playing" class="playing">
                    <?php
                        include 'nowplaying.php';
                    ?>
                </div>
                <h2>Playlist</h2>
                <ul>
                    <?php
                        $handle = fopen("../../home/pi/music/playlist/.playlist", "r");
                            if ($handle) {
                                while (($line = fgets($handle)) !== false) {
                                    $playSep = explode(' /',$line);
                                    $playTitlelong = explode('/music/',$line);
                                    $playTitle1 = str_replace ("_", " ", $playTitlelong[1]);
                                    $playTitle = str_replace (".mp3", "", $playTitle1);
                                    $varTitle = explode(' - ',$playTitle);
                                    $r = 'remove';
                                    $cult = $a . '' . $m;
                                    $remov = $playTitle1[0] . '' . $playTitle1[1] .'' . $playTitle1[2] . '' . $r;
                                    echo "<li>$playTitle<form method='post' action=''><button id='refresh' class='remove' name=".$remov."><img class='remove' src='images/remove.png' /></button></form></li>";
                                    if(isset($_POST[$remov])){
                                        shell_exec("sudo sed -i '/$varTitle[0]/d' /home/pi/music/playlist/.playlist");
                                        echo("<script>window.location = 'index.php';</script>");
                                    }
                                }
                            }
                        fclose($handle);
                    ?>
                </ul>
                <form method="post">
                    <button class="startPlay" name="playlist">
                        Start
                    </button>
                </form>
            </div>
        </div>
    </body>
</html>